In CMDER:

λ %GOBIN%\packer build ubuntu_ansible_azure.json

λ az vm create --resource-group ex532ResourceGroup --name ex532VM2 --image ex532PackerUAAImage --admin-username admin001 --ssh-key-value @~/.ssh/id_rsa.pub

λ az vm open-port --resource-group ex532ResourceGroup --name ex532VM2 --port 80

In Cygwin:

$ ansible myserver -i myinventory --private-key /cygdrive/c/Users/MasterPlan-S03/.ssh/id_rsa --user=admin001 -a 'ls -l /'

$ ssh -i /cygdrive/c/Users/MasterPlan-S03/.ssh/id_rsa admin001@51.4.229.144

