# Execute Shell command ('-m shell' not required):
ansible server -i inventory --private-key /cygdrive/c/Users/MasterPlan-S03/.ssh/id_rsa --user=admin001 -a 'ls -l /'

# Copy File:
$ ansible server -i inventory --private-key /cygdrive/c/Users/MasterPlan-S03/.ssh/id_rsa --user=admin001 -m copy -a "src=./filecopytest dest=~/filecopytest"

# Change file permission:
$ ansible server -i inventory --private-key /cygdrive/c/Users/MasterPlan-S03/.ssh/id_rsa --user=admin001 -m file -a "dest=~/filecopytest mode=600"

# Checkout GIT repository:
$ ansible server -i inventory --private-key /cygdrive/c/Users/MasterPlan-S03/.ssh/id_rsa --user=admin001 -m git -a "repo=https://github.com/rmontag/springboot-yaml.git dest=~/springboot-yaml version=HEAD"

